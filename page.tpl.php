<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body>
<div id="wrap">
  <div id="header">
  <h1><a href="<?php print $base_path ?>"><?php print $site_name ?></a></h1>
  <h2><?php print $site_slogan; ?></h2>
  </div>
<div class="middle">
  <h2><?php print $title; ?></h2>
  <?php print $tabs; ?>
  <?php print $help ?>
  <?php if($messages): ?><div style="border: 4px soild #AAA; padding: 5px; background-color: #CCC; font-size: 12px"><?php print $messages ?></div><?php endif;?>
  <?php print $content; ?>
  <?php print $feed_icons; ?>
</div>
		
<div class="right">
  <?php print $sidebar_right; ?>
  <?php print $sidebar_left; ?>
</div>

<div id="clear"></div>

</div>

<div id="footer">
<?php print $footer_message ?>
</div>
<?php print $closure ?>
</body>
</html>